//
//  ViewController.swift
//  FirstSample
//
//  Created by StanleyChung on 2017/11/1.
//  Copyright © 2017年 PentiumNetwork. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let camera = GMSCameraPosition.camera(withLatitude: 25.046121, longitude: 121.515540, zoom: 17.0)
        let mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = "Hello World!"
        marker.isDraggable = true
        marker.appearAnimation = .pop
        marker.map = mapView
        
        view.addSubview(mapView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

